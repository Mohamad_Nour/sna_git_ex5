FROM python:3.8

WORKDIR /sna_git_ex5

COPY . /sna_git_ex5/

RUN pip install -r requirements.txt

ENTRYPOINT [ "python", "app.py" ]

